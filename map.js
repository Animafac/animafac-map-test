/*jslint browser: true*/
/*global L*/
if (typeof L !== "object") {
    throw "Leaflet not loaded correctly";
}
var map = L.map("map", {"minZoom": 6, "maxBounds": [[52.107, -14.678], [39.943, 14.326]]}).setView([46.28, 1.38], 6);

var layers = {
    "OpenStreetMap": L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"),
    "Wikimedia": L.tileLayer("https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png"),
    "Humanitarian OpenStreetMap": L.tileLayer("https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png"),
    "Stamen Toner": L.tileLayer("https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png"),
    "Stamen Watercolor": L.tileLayer("https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.png"),
    "Carto Light": L.tileLayer("https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png")
};
layers.OpenStreetMap.addTo(map);
L.control.layers(layers).addTo(map);
L.geoJson.ajax("arx_2017.geojson").addTo(map);
